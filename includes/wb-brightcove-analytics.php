<?php 
if (!class_exists('wb_brightcove_api_analytics')) {
	class wb_brightcove_api_analytics {

		protected $api_url;
		protected $proxy;

		function __construct($proxy = '', $api_url = '' ) {

			$this->api_url 	= $api_url;
			$this->proxy 	= $proxy;

		}

		public function query($data) {

        	$request = array( 'requestType'=>'GET', 'url' => $this->api_url ,'requestBody'=> $data);
        	$query = $this->proxy->create_request( $request );

        	return $query;
        }
	}
}