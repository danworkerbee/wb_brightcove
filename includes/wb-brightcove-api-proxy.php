<?php
	/**
	 * proxy for Brightcove RESTful APIs
	 * gets an access token, makes the request, and returns the response
	 * Accessing:
	 *     (note you should **always** access the proxy via HTTPS)
	 *     Method: POST
	 *     request body (accessed via php://input) is a JSON object with the following properties
	 *
	 * {string} url - the URL for the API request
	 * {string} [requestType=GET] - HTTP method for the request
	 * {string} [requestBody] - JSON data to be sent with write requests
	 * {string} [client_id] - OAuth2 client id with sufficient permissions for the request
	 * {string} [client_secret] - OAuth2 client secret with sufficient permissions for the request
	 *
	 * Example:
	 * {
	 *    "url": "https://cms.api.brightcove.com/v1/accounts/57838016001/video",
	 *    "requestType": "PATCH",
	 *    "client_id": "0072bebf-0616-442c-84de-7215bb176061",
	 *    "client_secret": "7M0vMete8vP_Dmb9oIRdUN1S5lrqTvgtVvdfsasd",
	 *    "requestBody": "{\"description\":\"Updated video description\"}"
	 * }
	 *
	 * if client_id and client_secret are not included in the request, default values will be used
	 *
	 * @returns {string} $response - JSON response received from the API
	 */
	

	if (!class_exists('wb_brightcove_api_proxy')) {

	    class wb_brightcove_api_proxy {      

			// default account values
			// if you work on one Brightcove account, put in the values below
			// if you do not provide defaults, the client id, and client secret must
			// be sent in the request body for each request
			protected $client_id;
			protected $client_secret;
			protected $curl_error;
			protected $api_url;
			
			function __construct($client_id='', $client_secret = '')
	        {
	           $this->client_id 	= $client_id;
	           $this->client_secret	= $client_secret;
	        }
			
			public function create_access_token() {

				if ($this->error()) return $this->error();

				$auth_string = "{$this->client_id}:{$this->client_secret}";
				
				// make the request to get an access token
				$request = "https://oauth.brightcove.com/v4/access_token?grant_type=client_credentials";
				$curl    = curl_init($request);

				curl_setopt($curl, CURLOPT_USERPWD, $auth_string);
				curl_setopt($curl, CURLOPT_POST, TRUE);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($curl, CURLOPT_HTTPHEADER, array(
					'Content-type: application/x-www-form-urlencoded',
				));
				
				$response = curl_exec($curl);
				$curl_info = curl_getinfo($curl);
				$php_log = array(
					"php_error_info" => $curl_info
				);
				$this->curl_error = curl_error($curl);
				
				curl_close($curl);

				return $response;
			}

			public function create_request( $data ) {
				$token = $this->create_access_token();

				if ( $token == FALSE ) return $this->curl_error;

				$access_token 	= json_decode($token);
				$access_token   = $access_token->access_token;
				$requestType 	= $data['requestType'];
				$request 		= $data['url'];
				$requestBody 	= $data['requestBody'];

				if ( $requestType == 'GET' ) {
					$query = $this->build_query($data['requestBody']);
					$request  = $data['url']. "?". $query;
				}

				$curl = curl_init($request);

				curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($curl, CURLOPT_HTTPHEADER, array(
					'Content-type: application/json',
					"Authorization: Bearer {$access_token}"
				));

				switch ($requestType)
				{
					case "POST":
						curl_setopt($curl, CURLOPT_POST, TRUE);
						if ($requestData->requestBody) {
							curl_setopt($curl, CURLOPT_POSTFIELDS, $requestBody);
						}
						break;
					case "PUT":
						// don't use CURLOPT_PUT; it is not reliable
						curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $requestType);
						if ($requestData->requestBody) {
							curl_setopt($curl, CURLOPT_POSTFIELDS, $requestBody);
						}
						break;
					case "PATCH":
						curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $requestType);
						if ($requestData->requestBody) {
							curl_setopt($curl, CURLOPT_POSTFIELDS, $requestBody);
						}
						break;
					case "DELETE":
						curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $requestType);
						if ($requestData->requestBody) {
							curl_setopt($curl, CURLOPT_POSTFIELDS, $requestBody);
						}
						break;
					default:
						// GET request, nothing to do;
				}

				$response = curl_exec($curl);
				$this->curl_error = curl_error($curl);
				$curl_info = curl_getinfo($curl);

				curl_close($curl);
				return $response == FALSE ? $this->curl_error : $response;
			}

			private function error() {
				if ( $this->client_id  == '' ) return 'Please Add Client ID';
				if ( $this->client_secret  == '' ) return 'Please Add Client Secret';
			}

			private function build_query($params ) {
				$paramsJoined = array();

				foreach($params as $param => $value) {
				   $paramsJoined[] = "$param=$value";
				}

				$query = implode('&', $paramsJoined);

				return $query;
			}
		}
	}	
