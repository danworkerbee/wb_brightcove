<?php
/**
 * Brightcove API Simplified Class For Wordpress
 *
 * @link       http://workerbee.tv/
 * @since      1.0.0
 *
 * @package    wb_brightcove
 * @subpackage wb_brightcove/vendor
 * @subpackage wb_brightcove/includes
 * @package    wb_brightcove
 * @author     Dan Ambas
 *
 * Resources & API Documentation
 * @link Analytics: https://apis.support.brightcove.com/analytics/getting-started/analytics-api-overview-dimensions-fields-and-parameters.html
*/

if (!class_exists('wb_brightcove_api')) {

    class wb_brightcove_api
    { 
    	protected $account_id;
        protected $client_id;
        protected $client_secret;
        protected $api_url;
        protected $proxy; 

        public $analytics;
        
        function __construct($client_id = '', $client_secret = '' ) {
           $this->client_id     = $client_id;
           $this->client_secret = $client_secret;
           $this->api_url 		= 'https://analytics.api.brightcove.com/v1/data';

           $this->includes();
           $this->instances();
        }

        private function includes() {
        	if (!defined('PLUGIN_DIR')) {
        		define('PLUGIN_DIR', plugin_dir_path(__FILE__));
        	}

        	if (!class_exists('wb_brightcove_api_proxy')) {
        		require_once PLUGIN_DIR . '/includes/wb-brightcove-api-proxy.php';
        	}

        	if (!class_exists('wb_brightcove_api_anayltics')) {
        		require_once PLUGIN_DIR . '/includes/wb-brightcove-analytics.php';
        	}
        }

        private function instances() {
        	$this->proxy 	 = new wb_brightcove_api_proxy($this->client_id, $this->client_secret);
            $this->analytics = new wb_brightcove_api_analytics( $this->proxy, $this->api_url );
        }
    }
}



$account_id = '77368806001';       
$client_id	   = 'ee292024-0e76-43c7-8c43-fb92d2cca508';       
$client_secret = 'LZzooekJPkPmtfZ54PG2Um7CeG9hnbRDd4VuD4WvsyfPzmQTMoSaGwWqUB88gn0qhj-GbSvOHAbXc7OJruqtcA';

$wb_brightcove = new wb_brightcove_api( $client_id, $client_secret);

$arraydata = array( 
	'accounts'=>$account_id, 
	'dimensions'=>'date', 
	'limit'=> 'all',
	'fields'=>'video_view, daily_unique_viewers',
	'from'=>'2020-07-10',
	'to'=>'2020-08-09',
	'format'=>"json"
);

print_r( $wb_brightcove->analytics->query( $arraydata ) );











